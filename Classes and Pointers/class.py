class Cookie:
    def __init__(self, color):
        # self is a method that is a part of a class
        self.color = color

    def get_color(self):
        return self.color

    def set_color(self):
        self.color = color


cookie_one = Cookie('green')
# the cookie itself is green
cookie_two = Cookie('blue')
# the cookie itself is blue
print("a is, ", cookie_one.get_color())
print("b is, ", cookie_two.get_color())

cookie_one.set_color('yellow')

print("a is now, ", cookie_one.get_color())
