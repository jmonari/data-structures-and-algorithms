#this is 11
num1 = 11

# now making num2 it 11
num2 = num1

# now making num1 22
num1 = 22

# value 11 inside a dictionary this is a pointer to a dictionary
dict1 = {
    'value1': 11
}
# this points the exact memory location of dict1
dict2 = dict1

print(dict1)
print(dict2)

dict1['value'] = 22

print(dict1)
print(dict2)
