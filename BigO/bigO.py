# Checks the time complexity of code
# time complexity - the number of operations to complete a code
# space complexity - prefers to memory space a code consumes

# 0(n) because is produces same output

def print_items(n):
    for i in range(n):
        print(i)

    for j in range(n):
        print(j)

# 0(n^2) because it just produces a product of n * n * n same as 0(n^2)


def print_items(n):
    for i in range(n):
        for j in range(n):
            for k in range(n):
                print(i, j)


def print_items(n):
    for i in range(n):
        for j in range(n):
            print(i, j)
    for k in range(n):
        print(k)


print_items(10)
