# Big O (Python)

**Checks the time complexity of code**

- Time complexity - the number of operations to complete a code
- Space complexity - prefers to memory space a code consumes

Lets say we have an array:

```python
a = [1,2,3,4,5,6,7]
accesing 1 will be the best case
accessing 4 will be the average case
accessing 7 will be the worst case
```

- Ω - Best Case(omega)
- θ - average case(Theta)
- 0 - worst case(Big0)

Big0 is always the worst case

**0(n)**

```python
def print_items(n):
    for i in range(n):
        print(i)

print_items(10)
```

0(n) - is **always** straight line [proportional]

# DROP CONSTANTS

```python
def print_items(n):
    for i in range(n):
        print(i)

    for j in range(n):
        print(j)

print_items(10)
```

This code is the **same as 0(n)** because it prints same [0-9] 2 times

**for loop inside a for loop**

```python
def print_items(n):
    for i in range(n):
        for j in range(n):
            print(i, j)

print_items(10)
```

n*n items = 0(n^2) [last item is 99]

```python
def print_items(n):
    for i in range(n):
        for j in range(n):
							for k in range(n):
				            print(i, j)

print_items(10)
```

n * n * n items = 0(n^2) [last item is 999]

<br>
<br>

# DROP NON-DOMINANTS

```python
# 0(n^2)
def print_items(n):
    for i in range(n):
        for j in range(n):
            print(i, j)
# 0(n)
    for k in range(n):
        print(k)

print_items(10)
```

- This expression is equals to **0(n^2 + n)**
- **n^2** is the **dominant** term in **0(n^2 + n)** and **n** is **non-dominant** so we just **drop** it to have **0(n^2)**

<br/>
<br/>

# 0(1) Constant Time

```python
def add_items(n):
		return n + n

```

In other operations, as **n gets bigger** the **operation increases**

- In this code if we have **n as a million,** operation the result is still one (1) meaning its just 0(1)
- It means even if **n increases the number of operations will still remain constant**
    
    []()
    
    ## 0(log n)
    
     
    
    ```python
    #find number 1 in the list must be sorted
    [1,2,3,4,5,6,7,8]    
    #divide the code in 2
    [1,2,3,4][5,6,7,8]
    #remove the half where 1 is not to be found result 1
    [1,2,3,4]   #step 1
    #devide into 2 again
    [1,2][3,4]
    #remove again result 2
    [1,2]      #step 2
    #remove again to have the result
    [1][2]
    #result 3
    [1]        #step 3
    ```
    
    - In this case $2^3 = 8$
        
        $log_2 8 = 3$
        
        $log_2 1,073,741,824 = 31$
        
        []()
        
          
        
        # Big 0 of Lists
        
        ```python
        mylist = [11,3,5,1,4]
        mylist.append(17) **#0(1)**
        mylist [11,3,5,1,4,17]
        mylist.pop() **#0(1)**
        mylist = [11,3,5,1,4]
        
        #but
        **#0(n)**
        mylist = [11,3,5,1,4]
        mylist.pop(0) # will need re-indexing like
        # 3 will move to 0 5 to 1 e.t.c
        mylist.insert(0,13) # There will be indexing needed
        # 3 will move back to index 1 and 5 will move to index 2 etc
        
        #else
        mylist = [11,3,5,1,4]
        mylist.insert(2,13)
        # this will need re-indexing same as **0(n)**
        
        ```
        
        # SUMMARY
        
        - $0(n^2)$ - loop within a loop
        - $0(n)$ - propotional always be a straight line
        - *0(log n) -* divide and conquer
        - *0(1) -* constant time

<br/>
<br/>

# LINKED LIST

- We have head next next to finally tail

**Linked list Big0**

- **appending 0(1)**
- **pop = 0(n)** because we have to pull the tail to the last value
- **add to the front = 0(1)**
- **Remove from the front = 0(1)**
- **insert in the middle = 0(n)**
- **Removing the middle = 0(n)**
- **look for a value = 0(n)**
<br/>
<br/>
<hr />

## node is the value + the pointer e.g (11)->(7)->(4)->
this makes up a dictionary

```python
head:{
    "value": 11,
    "next": {
        "value": 7,
        "next": {
            "value": 4,
tail:       "next": None
        }
    }
}

```